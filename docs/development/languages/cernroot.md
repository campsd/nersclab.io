# Cern Root

[ROOT](https://root.cern) is open source data analysis framework, maintaned by CERN, 
used mainly by the high energy physics community to analyze, plot, and store data.
ROOT is written in C++ and features a C++ REPL, cling, as well as
python bindings. To give users more flexibility in version selection and maintnaince, 
on Perlmutter we encouraging users to use one of the following methods to install ROOT.

## With a conda install 

The ROOT project maintains a build which can can be installed with 
[conda](https://root.cern/install/#conda). This is the recommendation if you also 
intend to use the root python binding or jupyter for your analysis.

```bash
nersc$ module load python
nersc$ mamba create -c conda-forge --name cernroot root
nersc$ conda activate cernroot
```

To test:

```bash
(cernroot) nersc$ root -b 
root [0]
```

Or in python:

```python
(cernroot) nersc$ python
>>> import ROOT
>>>
```

## Using a container

There are a number of containers built and supported by the root project 
which you can find on their [docker hub](https://hub.docker.com/r/rootproject/root). 
To use these images on perlmutter, you can use the shifter runtime.

```bash
nersc$ shifterimg pull rootproject/root:latest
nersc$ shifter --image=rootproject/root:latest /bin/bash
nersc$ root -b
root [0] 
```

Docker containers are also helpful if your analysis software is built on top of root. 
You can use the original image provied from root and build your analysis software inside 
the container, upload your custom container to an image registry, and then download it 
with shifterimg pull onto perlmutter. 

??? Dockerfile

	```docker
	FROM rootproject/root:6.26.10-conda

	RUN conda install uproot
	```

## Building root on perlmutter

If you need specific options for your root build, you can build on perlmutter. 
Currently xrootd is not supported with this built type on perlmutter. 
??? Example
	```bash
	nersc$ module load PrgEnv-gnu
	nersc$ module load python
	nersc$ git clone --depth=1 -b latest-stable https://github.com/root-project/root.git cernroot
	nersc$ cd cernroot
	nersc$ mkdir compile; cd compile
	nersc$ cmake .. -Dxrootd=OFF -Dbuiltin_xrootd=OFF -DCMAKE_CXX_STANDARD=17 -DCMAKE_INSTALL_PREFIX=/path/to/install/root
	nersc$ make -j8; make install
	```
