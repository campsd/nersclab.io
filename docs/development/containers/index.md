# Containers

Users are welcome and encouraged to use containers to run their software on
NERSC systems. To facilitate this, NERSC provides various container
technologies.

## `podman-hpc`

[`podman-hpc`](podman-hpc/overview.md), an adaptation of
[Podman](https://github.com/containers/podman) for HPC use-cases, is available
on Perlmutter and offers several advantages over the Shifter tool.

## Shifter

[Shifter](shifter/index.md) is a NERSC-developed tool which allows users to
run images on NERSC systems such as Perlmutter.

## `registry.nersc.gov`

NERSC offers a private image registry where users can store images. We offer
[information about and best practices for using the registry](registry/index.md).
