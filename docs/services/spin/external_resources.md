# External Resources

When developing containerized applications, there are several external resources and communities to draw knowledge from.
Here are a few starting places.

* [Docker Container Runtime Documentation](https://docs.docker.com/reference/)
* [Podman Container Runtime Documentation](https://docs.podman.io/en/latest/Reference.html)
* [Rancher Kubernetes Environment Documentation](https://ranchermanager.docs.rancher.com/)
* [Kubernetes Container Orchestration Documentation](https://kubernetes.io/docs/reference/)
* [Helm Documentation (Kubernetes Package Management)](https://helm.sh/docs/)
